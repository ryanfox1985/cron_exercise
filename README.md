# Cron parser

Add execution permissions and run:

```sh
chmod +x main.rb
./main.rb "*/15 0 1,15 * 1-5 /usr/bin/find"
```

Run tests:

```sh
bundle install
bundle exec rspec
```