class ParseCmd
  def self.call(input)
    %r{^[\w/].+$} =~ input.to_s ? input.to_s : nil
  end
end
