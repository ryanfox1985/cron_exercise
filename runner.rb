require './parse_args'
require './parse_expr'
require './parse_cmd'

class Runner
  def self.call(input)
    matcher = ParseArgs.call(input)
    exit(1) if matcher.nil?

    minute = ParseExpr.call(matcher[:minute], (0..59))
    hour = ParseExpr.call(matcher[:hour], (0..23))
    day_of_month = ParseExpr.call(matcher[:day_of_month], (1..31))
    month = ParseExpr.call(matcher[:month], (1..12))
    day_of_week = ParseExpr.call(matcher[:day_of_week], (0..6))
    command = ParseCmd.call(matcher[:command])

    exit(2) if minute.nil? || hour.nil? || day_of_month.nil? || month.nil? || day_of_week.nil? || command.nil?

    puts "minute\t\t#{minute}"
    puts "hour\t\t#{hour}"
    puts "day of month\t#{day_of_month}"
    puts "month\t\t#{month}"
    puts "day of week\t#{day_of_week}"
    puts "command\t\t#{command}"
  end
end
