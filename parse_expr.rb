class ParseExpr
  def self.call(input, range)
    return range.to_a.join(' ') if input == '*'

    if /^[\d+,?]+$/ =~ input
      return nil if input == ','

      return input.split(',').map { |v| range.include?(v.to_i) ? v : exit(2) }.join(' ')
    end

    matcher = %r{^*/(?<param>\d+)}.match(input)
    if matcher
      param = matcher[:param].to_i
      return range.select { |v| (v % param).zero? }.join(' ') if range.include?(param)

      return nil
    end

    matcher = /^(?<min>\d+)-(?<max>\d+)$/.match(input)
    if matcher && range.include?(matcher[:min].to_i) && range.include?(matcher[:max].to_i)
      return (matcher[:min]..matcher[:max]).to_a.join(' ')
    end

    nil
  end
end
