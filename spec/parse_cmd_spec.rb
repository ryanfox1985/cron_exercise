require './parse_cmd'

describe ParseCmd do
  subject { described_class.call(input) }
  context 'when valid input' do
    context 'when /usr/bin/find' do
      let(:input) { '/usr/bin/find' }
      it 'returns command' do
        expect(subject).to eq('/usr/bin/find')
      end
    end

    context 'when echo "hello world"' do
      let(:input) { 'echo "hello world"' }
      it 'returns command' do
        expect(subject).to eq('echo "hello world"')
      end
    end
  end

  context 'when invalid input' do
    let(:input) { '* /usr/bin/find' }
    it 'returns nil' do
      expect(subject).to be_nil
    end
  end
end
