require './runner'
require './parse_args'
require './parse_cmd'
require './parse_expr'

describe Runner do
  subject { described_class.call('*/15 0 1,15 * 1-5 /usr/bin/find') }
  context 'when valid input' do
    before do
      allow(ParseArgs).to receive(:call).and_return({})
      allow(ParseExpr).to receive(:call).and_return('value')
      allow(ParseCmd).to receive(:call).and_return('value')
    end

    it 'exits successfully' do
      expect { subject }.to_not raise(SystemExit)
    end
  end

  context 'when invalid input' do
    context 'fail args' do
      before do
        allow(ParseArgs).to receive(:call).and_return(nil)
      end

      it 'returns nil' do
        expect { subject }.to raise_error(SystemExit) do |error|
          expect(error.status).to eq(1)
        end
      end
    end

    context 'fail expr' do
      before do
        allow(ParseArgs).to receive(:call).and_return({})
        allow(ParseExpr).to receive(:call).and_return(nil)
      end

      it 'returns nil' do
        expect { subject }.to raise_error(SystemExit) do |error|
          expect(error.status).to eq(2)
        end
      end
    end

    context 'fail cmd' do
      before do
        allow(ParseArgs).to receive(:call).and_return({})
        allow(ParseExpr).to receive(:call).and_return('value')
        allow(ParseCmd).to receive(:call).and_return(nil)
      end

      it 'returns nil' do
        expect { subject }.to raise_error(SystemExit) do |error|
          expect(error.status).to eq(2)
        end
      end
    end
  end
end
