require './parse_expr'

describe ParseExpr do
  let(:range) { (0..5) }
  subject { described_class.call(input, range) }

  context 'when valid input' do
    context '*' do
      let(:input) { '*' }

      it 'returns all values' do
        expect(subject).to eq('0 1 2 3 4 5')
      end
    end

    context '*/2' do
      let(:input) { '*/2' }

      it 'returns pairs values' do
        expect(subject).to eq('0 2 4')
      end
    end

    context '0' do
      let(:input) { '0' }

      it 'returns selected value' do
        expect(subject).to eq('0')
      end
    end

    context '1,3' do
      let(:input) { '1,3' }

      it 'returns selected values' do
        expect(subject).to eq('1 3')
      end
    end

    context '1-3' do
      let(:input) { '1-3' }

      it 'returns selected values' do
        expect(subject).to eq('1 2 3')
      end
    end
  end

  context 'when invalid input' do
    context 'random' do
      let(:input) { 'abc' }

      it 'returns nil' do
        expect(subject).to be_nil
      end
    end

    context '*' do
      let(:input) { '**' }

      it 'returns nil' do
        expect(subject).to be_nil
      end
    end

    context '*/2' do
      context 'with */100' do
        let(:input) { '*/100' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end

      context 'with 100/*' do
        let(:input) { '100/*' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end

      context 'with */*' do
        let(:input) { '*/*' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end

      context 'with */*/100' do
        let(:input) { '*/*' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end
    end

    context '0' do
      let(:input) { '100' }

      it 'returns nil' do
        expect(subject).to be_nil
      end
    end

    context '1,15' do
      context 'when 1,15' do
        let(:input) { '1,15' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end

      context 'when 1,15,' do
        let(:input) { '1,15,' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end

      context 'when ,1,15' do
        let(:input) { ',1,15' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end

      context 'when ,' do
        let(:input) { ',' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end
    end

    context '1-15' do
      context 'when 1-15' do
        let(:input) { '1-15' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end

      context 'when -15' do
        let(:input) { '-15' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end

      context 'when 15-' do
        let(:input) { '15-' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end

      context 'when 15-,10' do
        let(:input) { '15-,10' }

        it 'returns nil' do
          expect(subject).to be_nil
        end
      end
    end
  end
end
