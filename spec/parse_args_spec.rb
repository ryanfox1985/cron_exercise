require './parse_args'

describe ParseArgs do
  subject { described_class.call(input) }
  context 'when valid input' do
    let(:input) { '*/15 0 1,15 * 1-5 /usr/bin/find' }
    it 'returns matchers' do
      expect(subject).to be_a(MatchData)
    end
  end

  context 'when invalid input' do
    let(:input) { '*/15 0 1,15 1-5 /usr/bin/find' }
    it 'returns nil' do
      expect(subject).to be_nil
    end
  end
end
