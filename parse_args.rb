class ParseArgs
  def self.call(input)
    %r{^(?<minute>[*\d/,-]+) (?<hour>[*\d/,-]+) (?<day_of_month>[*\d/,-]+) (?<month>[*\d/,-]+) (?<day_of_week>[*\d/,-]+) (?<command>.+)$}.match(input.to_s)
  end
end
